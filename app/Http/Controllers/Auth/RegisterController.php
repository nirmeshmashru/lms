<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/
	
	use RegistersUsers;
	
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $registerView = 'auth.register';
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
        request()->request->add(['user_id' => '0']);
	}
	
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
        $validator = Validator::make($data, [
			'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
		]);

        //code for lms create user
        $data['apikey']= env('API_KEY');
        $data['apicode']= env('API_CODE');
        $data['task']= 'createuser';
        $data['type']= 'json';

        $client = new Client();
        $result = $client->request('POST', env('LMS_URL'), [
            'query' => $data
        ]);
        $response = json_decode($result->getBody());

        $validator->after(function ($validator) use ($response){
            if($response->createuser->result->{'-status'} == 0){
                $validator->errors()->add('email','Email already exists');
            }else{
                request()->request->add(['user_id' => $response->createuser->result->{'userID'}]);
            }
        });

        return $validator;
	}

	
	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		return User::create([
			'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
			'email' => $data['email'],
            'lms_user_id' => $data['user_id'],
			'password' => bcrypt($data['password']),
		]);
	}
}
